/* eslint-disable eqeqeq */
/* eslint-disable react/style-prop-object */
/* eslint-disable import/no-anonymous-default-export */
import React from 'react';
import { notification } from 'antd';
import { CloseCircleFilled } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
import s from './styles.module.scss'

const openNotification = (placement: any, message: any, icon: any) => {
    notification.info({
        message: `Notification`,
        description: message,
        placement,
        icon: icon,
    });
};

const MovieItem = ({ name, year, img, genre = '', age, id, type = true }: { name: any, year: any, img: any, genre: any, age: any, id: any, type?: any }) => {
    const handleClickFalse = (event: any) => {
        openNotification('topRight', 'Please log in to watch movies', <CloseCircleFilled style={{ color: 'red' }} />);
        event.preventDefault();
    };

    const navigate = useNavigate();

    return (
        <div className={s.item}>
            <article onClick={type ? (() => { navigate(`/movie-details-page/${id}`) }) : handleClickFalse}>
                <div className={s.poster}>
                    <img src={img} alt="" />
                    <div className={s.feature}>{genre || 'Khác'}</div>
                </div>
                <div className={s.data}>
                    <h3>
                        <a href={'/movie-details-page/' + id} onClick={type ? (() => { }) : handleClickFalse}>{name}</a>
                    </h3>
                    <span>Year: {year}</span>
                    <span>Age: {age}</span>
                </div>
            </article>
        </div>
    )
}

export default MovieItem;