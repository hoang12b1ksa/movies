import React from 'react';
import ReactDOM from 'react-dom/client';
import { Helmet } from 'react-helmet';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Routes from './routers/index';
import { Input, initMDB } from 'mdb-ui-kit';
import '../node_modules/mdb-ui-kit/css/mdb.min.css';

initMDB({ Input });

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);

root.render(
  <>
    <Helmet titleTemplate="%s" defaultTitle="Web xem phim">
      <meta name="description" content="Web xem phim" />
    </Helmet>

    <Routes />
  </>

);

reportWebVitals();
