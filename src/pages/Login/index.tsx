/* eslint-disable eqeqeq */
/* eslint-disable react/style-prop-object */
/* eslint-disable import/no-anonymous-default-export */
import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Input, Form, notification } from 'antd';
import s from './styles.module.scss';
import axios from 'axios';
import { CheckCircleFilled, CloseCircleFilled } from '@ant-design/icons';

export default () => {
    const [form] = Form.useForm();
    const navigate = useNavigate();

    const openNotification = (placement: any, message: any, icon: any) => {
        notification.info({
            message: `Notification`,
            description: message,
            placement,
            icon: icon,
        });
    };

    const login = async (values: any) => {
        try {
            const body = values;

            const config = {
                headers: {
                    'Content-Type': 'application/json'
                },
            };

            const response = await axios.post("http://43.239.223.55:6068/login", body, config)

            if (response?.data?.access_token) {
                openNotification('topRight', "Login Successfull", <CheckCircleFilled style={{ color: 'green' }} />);
                localStorage.setItem('access_token', response?.data?.access_token);
                localStorage.setItem('name', response?.data?.user?.name);
                setTimeout(() => {
                    navigate('/home-page');
                }, 3000);
            }
            else {
                openNotification('topRight', "An unknown error", <CloseCircleFilled style={{ color: 'red' }} />);
            }

        }
        catch (err: any) {
            openNotification('topRight', err?.response?.data?.message, <CloseCircleFilled style={{ color: 'red' }} />);
        }

    }

    const handleLogin = (values: any) => {
        const { email, password } = values;
        login({ email, password })
    };

    const noAccentedChars = /^[\u0000-\u007F]+$/;

    useEffect(() => {
        const access_token = localStorage.getItem('access_token');

        if (access_token)
            navigate('/home-page');
    }, [])

    return (
        <>
            <section className="vh-100 bg-image" style={{ minHeight: "676px", backgroundImage: "url('')", overflowY: 'auto' }}>
                <div className="mask d-flex align-items-center h-100 gradient-custom-3">
                    <div className="container h-100">
                        <div className="row d-flex justify-content-center align-items-center h-100">
                            <div className="col-12 col-md-9 col-lg-7 col-xl-6">
                                <div className="card" style={{ borderRadius: '15px' }}>
                                    <div className="card-body p-5">
                                        <h2 className="text-uppercase text-center mb-5">Login</h2>

                                        <Form
                                            name="basic"
                                            form={form}
                                            onFinish={handleLogin}
                                            autoComplete="off"
                                        >
                                            <div className="form-outline mb-4">
                                                <Form.Item name='email' rules={[{ required: true, message: 'Please input your email!' }]}>
                                                    <Input
                                                        type="email"
                                                        placeholder="Email"
                                                        className={s.font_antd}
                                                    />
                                                </Form.Item>
                                            </div>

                                            <div className="form-outline mb-4">
                                                <Form.Item
                                                    name="password"
                                                    rules={[
                                                        { required: true, message: 'Please input your password!' },
                                                        () => ({
                                                            validator(_, value) {
                                                                if (!value || noAccentedChars.test(value)) {
                                                                    return Promise.resolve();
                                                                }
                                                                return Promise.reject(new Error('Password must not contain accented characters.'));
                                                            },
                                                        }),
                                                    ]}
                                                >
                                                    <Input.Password
                                                        placeholder="Password"
                                                        className={s.font_antd}
                                                    />
                                                </Form.Item>
                                            </div>

                                            <div className="d-flex justify-content-center">
                                                <button
                                                    type="submit"
                                                    className="btn btn-success btn-block btn-lg gradient-custom-4 text-body"
                                                >
                                                    Login
                                                </button >
                                            </div>

                                            <p className="text-center text-muted mt-5 mb-0">
                                                Haven't already an account? <a href="/sign-up" className="fw-bold text-body"><u>Create an account</u></a>
                                            </p>
                                        </Form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
};
