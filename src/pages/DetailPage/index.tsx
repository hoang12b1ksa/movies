/* eslint-disable jsx-a11y/heading-has-content */
/* eslint-disable eqeqeq */
/* eslint-disable react/style-prop-object */
/* eslint-disable import/no-anonymous-default-export */
import React, { useEffect, useState } from 'react';
import { Button } from 'antd';
import { useParams } from 'react-router-dom';
import { notification } from 'antd';
import { useNavigate } from 'react-router-dom';
import { CloseCircleFilled, CheckCircleFilled } from '@ant-design/icons';
import { GET_MOVIE_BY_ID, MARK_WATCHED } from "../../api/baseApi";
import s from './styles.module.scss';

const openNotification = (placement: any, message: any, icon: any) => {
    notification.info({
        message: `Notification`,
        description: message,
        placement,
        icon: icon,
    });
};

export default () => {
    const access_token = localStorage.getItem('access_token');
    const name = localStorage.getItem('name');
    const navigate = useNavigate();
    const { id } = useParams();

    const [info, setInfo]: any = useState({});

    const handleLogout = () => {
        localStorage.clear();
        window.location.reload();
    }

    const getStart = async () => {
        const info = await GET_MOVIE_BY_ID(id);
        if (info == null) {
            openNotification('topRight', 'This movie could not be found...', <CloseCircleFilled style={{ color: 'red' }} />);
            navigate('/hone-page');
        }
        else {
            setInfo(info);
        }
    }

    const handlePlay = async () => {
        await MARK_WATCHED(id);
        window.location.reload();
    }

    const handleReplay = async () => {
        openNotification('topRight', 'Đã xem', <CheckCircleFilled style={{ color: 'green' }} />);
    }

    useEffect(() => {
        if (!access_token) {
            navigate('/home-page');
        }
        else {
            getStart();
        }
    }, [])

    return (
        <section className="vh-100 bg-image" style={{ minHeight: "676px", backgroundImage: "url('')", overflowY: 'auto' }}>

            <div className={s.header}>
                <div className={s.m_container}>
                    <a href="/#" className={s.logo_container}>
                        <div className={s.full_logo}>
                            <img loading='lazy' alt="Mioto" src="/logo.png" />
                        </div>
                    </a>
                    <div className={s.menu_container}>
                        {access_token ? (
                            <>
                                <a href="/watched-movie-page">Phim đã xem</a>
                                <div className={s.vertical_line}></div>
                                <div className={s.dropdown_profile}>
                                    <div>
                                        <div className={s.avatar}>
                                            <img src="https://n1-astg.mioto.vn/g/2024/05/06/22/A5_2LvRfl7C0_I46aIIN5g.jpg" alt="" />
                                        </div>
                                        <div className={s.info}>
                                            <span>{name}</span>
                                            <Button type="primary" style={{ height: '24px', padding: '0px' }} onClick={handleLogout} danger>
                                                Log out
                                            </Button>
                                        </div>
                                    </div>
                                </div>
                            </>
                        ) : (
                            <>
                                <a className={`${s.sign_in} ${s.btn} ${s.btn__s}`} href="/sign-up">Đăng ký</a>
                                <a className={`${s.login} ${s.btn} ${s.btn__s}`} href="/login">Đăng nhập</a>
                            </>
                        )}
                    </div>
                </div>
            </div>

            <div className="gradient-custom-3" style={{ width: "100%", display: "flex", flexDirection: "column", minHeight: '100vh' }}>
                <div className={s.m_container}>
                    <div className={s.movie_image}>
                        <div className={s.img}>
                            <img src={info?.image} alt="" />
                        </div>

                        <div className={s.group_button}>
                            {info?.seen ? (
                                <>
                                    <button style={{ backgroundColor: '#ff3860', cursor: 'not-allowed' }}>Watched</button>
                                    <button onClick={handleReplay} style={{ backgroundColor: '#bd10e0', cursor: 'pointer' }}>Replay</button>
                                </>
                            ) : (
                                <button onClick={handlePlay} style={{ backgroundColor: '#8bc34a', cursor: 'pointer' }}>Play</button>
                            )}

                        </div>
                    </div>
                    <div className={s.moive_info}>
                        <h1>{info?.name}</h1>
                        <div className={s.txt}>
                            <ul>
                                <li>
                                    <p>Awards: </p>
                                    <h2>{info?.awards || 'Đang cập nhật'}</h2>
                                </li>
                                <li>
                                    <p>Actors: </p>
                                    <h2>{info?.actors?.join(', ') || 'Đang cập nhật'}</h2>
                                </li>
                                <li>
                                    <p>Director: </p>
                                    <h2>{info?.director?.join(', ') || 'Đang cập nhật'}</h2>
                                </li>
                                <li>
                                    <p>Date Created: </p>
                                    <h2>{info?.dateCreated || 'Đang cập nhật'}</h2>
                                </li>
                                <li>
                                    <p>Content Rating: </p>
                                    <h2>{info?.contentRating || 'Đang cập nhật'}</h2>
                                </li>

                                <li>
                                    <p>Year: </p>
                                    <h2>{info?.yearCreated || 'Đang cập nhật'}</h2>
                                </li>
                                <li>
                                    <p>Type: </p>
                                    <h2>{info?.type || 'Đang cập nhật'}</h2>
                                </li>
                                <li>
                                    <p>Genre: </p>
                                    <div className={s.genre}>{info?.genre || 'Đang cập nhật'}</div>
                                </li>
                                <li>
                                    <p style={{ width: '110px' }}>Description: </p>
                                    <h2>{info?.description || 'Đang cập nhật'}</h2>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className={s.movie_other}></div>
            </div>
        </section >
    );
};
