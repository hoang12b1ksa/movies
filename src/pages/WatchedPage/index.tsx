/* eslint-disable eqeqeq */
/* eslint-disable react/style-prop-object */
/* eslint-disable import/no-anonymous-default-export */
import React, { useEffect, useState } from 'react';
import { FrownOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import { useNavigate } from 'react-router-dom';
import { GET_MOVIE_SUGGEST, GET_MOVIE_WATCHED } from "../../api/baseApi";
import s from './styles.module.scss';

import MovieItem from '../../components/HomePage/MovieItem';

export default () => {
    const access_token = localStorage.getItem('access_token');
    const name = localStorage.getItem('name');
    const navigate = useNavigate();

    const [listWatched, setListWatched] = useState([]);
    const [listSuggest, setListSuggest] = useState([]);

    const handleLogout = () => {
        localStorage.clear();
        window.location.reload();
    }

    const getStart = async () => {
        const listSuggest = await GET_MOVIE_SUGGEST();
        const listWatched = await GET_MOVIE_WATCHED();
        setListSuggest(listSuggest);
        setListWatched(listWatched);
    }

    useEffect(() => {
        if (!access_token) {
            navigate('/home-page');
        }
        else {
            getStart();
        }
    }, [])

    return (
        <section className="vh-100 bg-image" style={{ minHeight: "676px", backgroundImage: "url('')", overflowY: 'auto' }}>

            <div className={s.header}>
                <div className={s.m_container}>
                    <a href="/#" className={s.logo_container}>
                        <div className={s.full_logo}>
                            <img loading='lazy' alt="Mioto" src="/logo.png" />
                        </div>
                    </a>
                    <div className={s.menu_container}>
                        <a href="/home-page">Trang chủ</a>
                        <div className={s.vertical_line}></div>

                        {access_token ? (
                            <div className={s.dropdown_profile}>
                                <div>
                                    <div className={s.avatar}>
                                        <img src="https://n1-astg.mioto.vn/g/2024/05/06/22/A5_2LvRfl7C0_I46aIIN5g.jpg" alt="" />
                                    </div>
                                    <div className={s.info}>
                                        <span>{name}</span>
                                        <Button type="primary" style={{ height: '24px', padding: '0px' }} onClick={handleLogout} danger>
                                            Log out
                                        </Button>
                                    </div>
                                </div>
                            </div>
                        ) : (
                            <>
                                <a className={`${s.sign_in} ${s.btn} ${s.btn__s}`} href="/sign-up">Đăng ký</a>
                                <a className={`${s.login} ${s.btn} ${s.btn__s}`} href="/login">Đăng nhập</a>
                            </>
                        )}
                    </div>
                </div>
            </div>

            <div className="gradient-custom-3" style={{ width: "100%", display: "flex", flexDirection: "column", minHeight: '100vh' }}
            >
                <div className={s.list_movie}>
                    <header>
                        <h2>Phim đã xem</h2>
                    </header>

                    {listWatched?.length > 0 ? (
                        <div className={s.list}>
                            {listWatched?.map((item: any) => (
                                <MovieItem
                                    key={item.id}
                                    name={item.name}
                                    year={item.year}
                                    genre={item.genre}
                                    age={item.age}
                                    img={item.img}
                                    id={item.id} />
                            ))}
                        </div>
                    ) : (
                        <div className={s.no_suggest}>
                            <FrownOutlined style={{ fontSize: '24px', verticalAlign: 'middle', color: 'green' }} />
                            <h1> Bạn chưa xem phim nào ! </h1>
                            <FrownOutlined style={{ fontSize: '24px', verticalAlign: 'middle', color: 'red' }} />
                        </div>
                    )}
                </div>

                {access_token ? (<div className={s.list_movie}>
                    <header>
                        <h2>Danh sách các gợi ý</h2>
                    </header>

                    {listSuggest?.length > 0 ? (
                        <div className={s.list}>
                            {listSuggest?.map((item: any) => (
                                <MovieItem
                                    key={item.id}
                                    name={item.name}
                                    year={item.year}
                                    genre={item.genre}
                                    age={item.age}
                                    img={item.img}
                                    id={item.id} />
                            ))}
                        </div>
                    ) : (
                        <div className={s.no_suggest}>
                            <FrownOutlined style={{ fontSize: '24px', verticalAlign: 'middle', color: 'green' }} />
                            <h1> Chưa có gợi ý nào cho bạn ! </h1>
                            <FrownOutlined style={{ fontSize: '24px', verticalAlign: 'middle', color: 'red' }} />
                        </div>
                    )}
                </div>) : (
                    <></>
                )}
            </div>
        </section >
    );
};
