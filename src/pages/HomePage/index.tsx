/* eslint-disable eqeqeq */
/* eslint-disable react/style-prop-object */
/* eslint-disable import/no-anonymous-default-export */
import React, { useEffect, useState } from 'react';
import { FrownOutlined } from '@ant-design/icons';
import { Button, Select, Pagination } from 'antd';
import { GET_MOVIE_ALL, GET_MOVIE_GENRE, GET_MOVIE_SUGGEST, GET_TOTAL_ALL, GET_TOTAL_GENRE } from "../../api/baseApi";
import s from './styles.module.scss';

import MovieItem from '../../components/HomePage/MovieItem';

export default () => {
    const access_token = localStorage.getItem('access_token');
    const name = localStorage.getItem('name');

    const [totalAll, setTotalAll] = useState(0);
    const [totalGenre, setTotalGenre] = useState(0);

    const [curentPageAll, setCurrentPageAll] = useState(1);
    const [currentPageGenre, setCurrentPageGenre] = useState(1);

    const [genre, setGenre] = useState(1);

    const [listAll, setListAll] = useState([]);
    const [listGenre, setListGenre] = useState([]);
    const [listSuggest, setListSuggest] = useState([]);

    const handleLogout = () => {
        localStorage.clear();
        window.location.reload();
    }

    const handlePaginationChange = (pageNumber: any, content: any) => {
        if (content == 'all') {
            setCurrentPageAll(pageNumber)
        }

        if (content == 'genre') {
            setCurrentPageGenre(pageNumber)
        }
    };

    const handleSelectChange = (value: any) => {
        setGenre(value);
    };

    const getStart = async () => {
        const totalAll = await GET_TOTAL_ALL();
        const totalGenre = await GET_TOTAL_GENRE(genre);
        const listAll = await GET_MOVIE_ALL(curentPageAll);
        const listGenre = await GET_MOVIE_GENRE(genre, currentPageGenre);
        const listSuggest = await GET_MOVIE_SUGGEST();

        setTotalGenre(totalGenre);
        setTotalAll(totalAll);
        setListAll(listAll);
        setListGenre(listGenre);
        setListSuggest(listSuggest);
    }

    const getUpdateList = async (content: any) => {
        if (content == 'all') {
            const listAll = await GET_MOVIE_ALL(curentPageAll);
            setListAll(listAll);
        }
        if (content == 'genre') {
            const listGenre = await GET_MOVIE_GENRE(genre, currentPageGenre);
            setListGenre(listGenre);
        }
    }

    const getUpdateTotalGenre = async () => {
        const totalGenre = await GET_TOTAL_GENRE(genre);
        setTotalGenre(totalGenre);
    }

    useEffect(() => {
        getUpdateList('all')
    }, [curentPageAll])

    useEffect(() => {
        getUpdateList('genre')
    }, [currentPageGenre])

    useEffect(() => {
        getUpdateTotalGenre();
        getUpdateList('genre');
    }, [genre])

    useEffect(() => {
        getStart();
    }, [])

    return (
        <section className="vh-100 bg-image" style={{ minHeight: "676px", backgroundImage: "url('')", overflowY: 'auto' }}>

            <div className={s.header}>
                <div className={s.m_container}>
                    <a href="/#" className={s.logo_container}>
                        <div className={s.full_logo}>
                            <img loading='lazy' alt="Mioto" src="/logo.png" />
                        </div>
                    </a>
                    <div className={s.menu_container}>
                        {access_token ? (
                            <>
                                <a href="/watched-movie-page">Phim đã xem</a>
                                <div className={s.vertical_line}></div>
                                <div className={s.dropdown_profile}>
                                    <div>
                                        <div className={s.avatar}>
                                            <img src="https://n1-astg.mioto.vn/g/2024/05/06/22/A5_2LvRfl7C0_I46aIIN5g.jpg" alt="" />
                                        </div>
                                        <div className={s.info}>
                                            <span>{name}</span>
                                            <Button type="primary" style={{ height: '24px', padding: '0px' }} onClick={handleLogout} danger>
                                                Log out
                                            </Button>
                                        </div>
                                    </div>
                                </div>
                            </>
                        ) : (
                            <>
                                <a className={`${s.sign_in} ${s.btn} ${s.btn__s}`} href="/sign-up">Đăng ký</a>
                                <a className={`${s.login} ${s.btn} ${s.btn__s}`} href="/login">Đăng nhập</a>
                            </>
                        )}
                    </div>
                </div>
            </div>

            <div className="gradient-custom-3" style={{ width: "100%", display: "flex", flexDirection: "column", minHeight: '100vh' }}
            >

                <div className={s.list_movie}>
                    <header>
                        <h2>Tất cả các phim</h2>
                    </header>

                    <div className={s.list}>
                        {listAll?.map((item: any) => (
                            <MovieItem
                                key={item.id}
                                name={item.name}
                                year={item.year}
                                genre={item.genre}
                                age={item.age}
                                img={item.img}
                                id={item.id}
                                type={access_token ? true : false}
                            />
                        ))}
                    </div>

                    <div className={totalAll > 20 ? s.pagination : s.none}>
                        <Pagination
                            style={{ float: 'right' }}
                            showQuickJumper
                            defaultPageSize={1}
                            defaultCurrent={1}
                            total={totalAll}
                            onChange={(pageNumber) => handlePaginationChange(pageNumber, 'all')}
                            showSizeChanger={false}
                        />
                    </div>
                </div>

                {access_token ? (<div className={s.list_movie}>
                    <header>
                        <h2>Danh sách các gợi ý</h2>
                    </header>

                    {listSuggest?.length > 0 ? (
                        <div className={s.list}>
                            {listSuggest?.map((item: any) => (
                                <MovieItem
                                    key={item.id}
                                    name={item.name}
                                    year={item.year}
                                    genre={item.genre}
                                    age={item.age}
                                    img={item.img}
                                    id={item.id}
                                    type={access_token ? true : false}
                                />
                            ))}
                        </div>
                    ) : (
                        <div className={s.no_suggest}>
                            <FrownOutlined style={{ fontSize: '24px', verticalAlign: 'middle', color: 'green' }} />
                            <h1> Chưa có gợi ý nào cho bạn ! </h1>
                            <FrownOutlined style={{ fontSize: '24px', verticalAlign: 'middle', color: 'red' }} />
                        </div>
                    )}
                </div>) : (
                    <></>
                )}

                <div className={s.list_movie}>
                    <header>
                        <h2>Phim theo thể loại</h2>
                        <div className={s.select_feature}>
                            <Select
                                defaultValue="1"
                                style={{ width: 200 }}
                                onChange={handleSelectChange}
                                options={[
                                    { value: '1', label: 'Action' },
                                    { value: '2', label: 'Adventure' },
                                    { value: '3', label: 'Anime' },
                                    { value: '4', label: 'Comedy' },
                                    { value: '5', label: 'Documentary' },
                                    { value: '6', label: 'Drama' },
                                    { value: '7', label: 'Fantasy' },
                                    { value: '8', label: 'Friendship' },
                                    { value: '9', label: 'Horror' },
                                    { value: '10', label: 'Kids' },
                                    { value: '11', label: 'Music' },
                                    { value: '12', label: 'Musical' },
                                    { value: '13', label: 'Reality TV' },
                                    { value: '14', label: 'Romance' },
                                    { value: '15', label: 'Sci-Fi' },
                                    { value: '16', label: 'Science' },
                                    { value: '17', label: 'Special Interest' },
                                    { value: '18', label: 'Sports' },
                                    { value: '19', label: 'Talk Show' },
                                    { value: '20', label: 'Thriller' },
                                    { value: '21', label: 'Variety TV' },
                                    { value: '22', label: 'Western' }
                                ]}
                            />
                        </div>
                    </header>

                    <div className={s.list}>
                        {listGenre?.map((item: any) => (
                            <MovieItem
                                key={item.id}
                                name={item.name}
                                year={item.year}
                                genre={item.genre}
                                age={item.age}
                                img={item.img}
                                id={item.id}
                                type={access_token ? true : false}
                            />
                        ))}
                    </div>

                    <div className={totalGenre > 20 ? s.pagination : s.none}>
                        <Pagination
                            style={{ float: 'right' }}
                            showQuickJumper
                            defaultPageSize={1}
                            defaultCurrent={1}
                            total={totalGenre}
                            onChange={(pageNumber) => handlePaginationChange(pageNumber, 'genre')}
                            showSizeChanger={false}
                        />
                    </div>
                </div>

            </div>
        </section >
    );
};
