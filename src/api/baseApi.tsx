import axios from "axios";
import { notification } from 'antd';
import { CloseCircleFilled } from '@ant-design/icons';

const openNotification = (placement: any, message: any, icon: any) => {
    notification.info({
        message: `Notification`,
        description: message,
        placement,
        icon: icon,
    });
};

const currentUrl = 'http://43.239.223.55:6068';

export const GET_MOVIE_ALL = async (page: any) => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        },
    };

    try {
        const res = await axios.get(`${currentUrl}/movies/all?page=${page}`, config);
        if (res?.status === 200) {
            return res?.data?.map((item: any) => {
                return {
                    name: item?.name || "",
                    year: item?.yearCreated || "",
                    genre: item?.genre || "",
                    age: item?.contentRating || "",
                    img: item?.image || "",
                    id: item?.id || ""
                }
            }) || []
        } else if (res?.status === 401) {
            openNotification('topRight', res?.data?.msg, <CloseCircleFilled style={{ color: 'red' }} />);
            localStorage.removeItem('access_token');
            return [];
        }
        else {
            return [];
        }
    } catch (error) {
        console.log(error);
        return [];
    }
};

export const GET_MOVIE_GENRE = async (genre: any, page: any) => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        },
    };

    try {
        const res = await axios.get(`${currentUrl}/movies/genre/${genre}?page=${page}`, config);
        if (res?.status === 200) {
            return res?.data?.map((item: any) => {
                return {
                    name: item?.name || "",
                    year: item?.yearCreated || "",
                    genre: item?.genre || "",
                    age: item?.contentRating || "",
                    img: item?.image || "",
                    id: item?.id || ""
                }
            }) || []
        } else if (res?.status === 401) {
            openNotification('topRight', res?.data?.msg, <CloseCircleFilled style={{ color: 'red' }} />);
            localStorage.removeItem('access_token');
            return [];
        }
        else {
            return [];
        }
    } catch (error) {
        console.log(error);
        return [];
    }
};

export const GET_MOVIE_SUGGEST = async () => {
    const currentToken = localStorage.getItem('access_token');

    const config = {
        headers: { Authorization: `Bearer ${currentToken}` },
    };

    if (!currentToken)
        return [];

    try {
        const res = await axios.get(`${currentUrl}/recommend`, config);
        if (res?.status === 200) {
            return res?.data?.map((item: any) => {
                return {
                    name: item?.name || "",
                    year: item?.yearCreated || "",
                    genre: item?.genre || "",
                    age: item?.contentRating || "",
                    img: item?.image || "",
                    id: item?.id || ""
                }
            }) || []
        } else if (res?.status === 401) {
            openNotification('topRight', res?.data?.msg, <CloseCircleFilled style={{ color: 'red' }} />);
            localStorage.removeItem('access_token');
            return [];
        }
    } catch (error: any) {
        console.log(error);
        openNotification('topRight', error?.response?.data?.msg, <CloseCircleFilled style={{ color: 'red' }} />);
        localStorage.removeItem('access_token');
        return [];
    }
};

export const GET_TOTAL_ALL = async () => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        },
    };

    try {
        const res = await axios.get(`${currentUrl}/movies/all/total_pages`, config);
        if (res?.status === 200) {
            return res?.data?.total_pages || 0;
        } else if (res?.status === 401) {
            openNotification('topRight', res?.data?.msg, <CloseCircleFilled style={{ color: 'red' }} />);
            localStorage.removeItem('access_token');
            return 0;
        }
        else {
            return 0;
        }
    } catch (error) {
        console.log(error);
        return 0;
    }
};

export const GET_TOTAL_GENRE = async (genre: any) => {
    const config = {
        headers: {
            'Content-Type': 'application/json'
        },
    };

    try {
        const res = await axios.get(`${currentUrl}/movies/genre/${genre}/total_pages`, config);
        if (res?.status === 200) {
            return res?.data?.total_pages || 0;
        } else if (res?.status === 401) {
            openNotification('topRight', res?.data?.msg, <CloseCircleFilled style={{ color: 'red' }} />);
            localStorage.removeItem('access_token');
            return 0;
        }
        else {
            return 0;
        }
    } catch (error) {
        console.log(error);
        return 0;
    }
};

export const GET_MOVIE_WATCHED = async () => {
    const currentToken = localStorage.getItem('access_token');
    const config = {
        headers: { Authorization: `Bearer ${currentToken}` },
    };

    try {
        const res = await axios.get(`${currentUrl}/watched`, config);
        if (res?.status === 200) {
            return res?.data?.map((item: any) => {
                return {
                    name: item?.name || "",
                    year: item?.yearCreated || "",
                    genre: item?.genre || "",
                    age: item?.contentRating || "",
                    img: item?.image || "",
                    id: item?.id || ""
                }
            }) || []
        } else if (res?.status === 401) {
            openNotification('topRight', res?.data?.msg, <CloseCircleFilled style={{ color: 'red' }} />);
            localStorage.removeItem('access_token');
            return [];
        }
    } catch (error) {
        console.log(error);
        return [];
    }
};

export const GET_MOVIE_BY_ID = async (id: any) => {
    const currentToken = localStorage.getItem('access_token');
    const config = {
        headers: { Authorization: `Bearer ${currentToken}` },
    };

    try {
        const res = await axios.get(`${currentUrl}/movie/${id}`, config);
        if (res?.status === 200) {
            return res?.data || null
        } else if (res?.status === 401) {
            openNotification('topRight', res?.data?.msg, <CloseCircleFilled style={{ color: 'red' }} />);
            localStorage.removeItem('access_token');
            return null;
        }
    } catch (error) {
        console.log(error);
        return null;
    }
};

export const MARK_WATCHED = async (id: any) => {
    const currentToken = localStorage.getItem('access_token');
    const config = {
        headers: { Authorization: `Bearer ${currentToken}` },
    };

    try {

        const res = await axios.post(`${currentUrl}/watch/${id}`, {}, config);
        if (res?.status === 200) {
            return true
        } else if (res?.status === 401) {
            openNotification('topRight', res?.data?.msg, <CloseCircleFilled style={{ color: 'red' }} />);
            localStorage.removeItem('access_token');
            return true;
        }
    } catch (error) {
        console.log(error);
        return false;
    }
}