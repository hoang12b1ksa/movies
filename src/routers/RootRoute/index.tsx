import React from 'react';
import { Navigate, useRoutes } from 'react-router';
import { Outlet } from 'react-router';
import App from '../../App';
import SignUp from '../../pages/SignUp';
import Login from '../../pages/Login';
import HomePage from '../../pages/HomePage';
import WatchedPage from '../../pages/WatchedPage';
import DetailPage from '../../pages/DetailPage';

const RootRoute = () => {

    return useRoutes([
        {
            path: '',
            element: <Navigate to={"home-page"} />,
        },
        {
            path: 'login',
            element: <Login />
        },
        {
            path: 'sign-up',
            element: <SignUp />
        },
        {
            path: 'home-page',
            element: <HomePage />
        },
        {
            path: 'movie-details-page/:id',
            element: <DetailPage />
        },
        {
            path: 'watched-movie-page',
            element: <WatchedPage />
        },
        {
            path: '*',
            element: <Navigate to={"home-page"} />,
        }
    ])
}

export default RootRoute